module BinaryPerceptron (BinaryPerceptron, Perceptron) where

data BinaryPerceptron = BinaryPerceptron {
    weights :: [Double]
} deriving (Eq, Show)

class Perceptron p where
    forward :: p -> [Double] -> Double
    backward :: p -> [Double] -> Double -> p

    forwardBatch :: p -> [[Double]] -> [Double]
    forwardBatch p x = [forward p (x!!i) | i <- [0..n-1]] where n = length x

    backwardBatch :: p -> [[Double]] -> [Double] -> p
    backwardBatch p [] [] = p
    backwardBatch p (x:xs) (y:ys) = backwardBatch pnew xs ys where pnew = backward p x y

    backwardEpochs :: p -> Int -> [[Double]] -> [Double] -> p
    backwardEpochs p 0 x y = p
    backwardEpochs p i x y = backwardEpochs pnew (i-1) x y where pnew = backwardBatch p x y 

instance Perceptron BinaryPerceptron where
    forward p x = signum(sum([(x!!i) * (w!!i) | i <- [0..n-1]])) where {
        n = length x;
        w = weights p
    }

    backward p x y = BinaryPerceptron wnext where {
        wnext = zipWith(+) w deltaW where {
            w = weights p;
            deltaW = map (* (y-o)) x where {
                o = forward p x
            }
        }
    }
