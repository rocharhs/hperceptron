:l binary_perceptron sandbox
p = BinaryPerceptron [0,0,0,0]
:module + Sandbox
dataCsv <- readCsvFromFile "data_banknote_authentication.txt"
(rows, columns) = shape dataCsv
x = getFirstColumns (columns-1) dataCsv
yraw = getColumn (columns-1) dataCsv
y = map (\x -> 2*(x-0.5)) yraw
pnew = backwardEpochs p 20 x y
o = forwardBatch pnew x
f1score = getF1score y o