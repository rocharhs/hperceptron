module Sandbox (readCsvFromFile, shape, getColumn, getFirstColumns, getF1score) where

splitOn :: Char -> String -> [String]

splitOn c s = case dropWhile (==c) s of
    "" -> []
    something -> h : splitOn c remainder
        where (h, remainder) = break (==c) something

splitLinesOn :: Char -> String -> [[String]]
splitLinesOn c s = map (splitOn c) (lines s)

readCsvFromStr :: String -> [[Double]]
readCsvFromStr s = [map (\x -> read x :: Double) (str_table!!i)|i <- [0..(n-1)]] where{
    str_table = splitLinesOn ',' s;
    n = length(str_table)
}

readCsvFromFile :: String -> IO [[Double]]
readCsvFromFile path = readCsvFromStr <$> readFile path

shape :: [[Double]] -> (Int, Int)
shape a = (length a, length (a!!0))

getColumn :: Int -> [[Double]] -> [Double]
getColumn c table = [((table!!i)!!c)|i <- [0..n-1]] where n = length table

getFirstColumns :: Int -> [[Double]] -> [[Double]]
getFirstColumns c table = [take c (table!!i) | i <- [0..n-1]] where n = length table

getF1score :: [Double] -> [Double] -> Double
getF1score y o = fromIntegral(2*correct)/fromIntegral(selected + target) where {
    correct = (length . filter (==True)) comparison where {
        comparison = [((o!!i) == 1) && ((o!!i) == (y!!i)) | i <- [0..n-1]] where n = length y
    };
    selected = (length . filter (==1)) o;
    target = (length . filter (==1)) y
}